{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Time Series Forecasting\n",
    "\n",
    "Time Series Forecasting differ from more traditional classification and regression predictive modeling problems. The temporal structure adds an order to the observations. This imposed order means that important assumptions about the consistency of these observations must be handled specifically.\n",
    "\n",
    "The goal of Time Series Forecasting is to make accurate predictions about the future. In this set of Jupyter notebooks, we will focus on how to make predictions on **univariate time series problems** using the standard tools in the **Python data science ecosystem**. [Pandas](http://pandas.pydata.org/) library in Python provides excellent, built-in support for time series data, while [Statsmodels](http://statsmodels.sourceforge.net/) is a Python module that allows users to explore data, estimate statistical models, and perform statistical tests. \n",
    "\n",
    "For learning Time Series Forecasting with Python, we recommend an excellent book *Introduction to Time Forecasting with Python* (2017) by Jason Brownlee. Several examples and techniques in this notebook are presented and explained in more details in that book.\n",
    "\n",
    "## Part III: Evaluation Framework"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# execute notebook with library imports\n",
    "%run libraries.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# check the versions of key python libraries\n",
    "print('pandas: %s' % pd.__version__)\n",
    "print('numpy: %s' % np.__version__)\n",
    "print('statsmodels: %s' % statsmodels.__version__)\n",
    "print('sklearn: %s' % sklearn.__version__)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# adjust display settings\n",
    "%matplotlib inline\n",
    "plt.rc('figure', figsize=(18, 3))\n",
    "plt.rcParams['figure.facecolor'] = 'w'\n",
    "pd.set_option('display.float_format', lambda x: '%.2f' % x)\n",
    "pd.options.display.max_rows = 30"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# CSV data\n",
    "csv_file = 'data/fed.csv'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load dataset\n",
    "dataframe = pd.read_csv(csv_file,\n",
    "                        index_col=0,\n",
    "                        parse_dates=True, \n",
    "                        date_parser=lambda date: pd.datetime.strptime(date, '%Y-%m-%d'),\n",
    "                       )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a time series\n",
    "s = pd.Series(dataframe.unstack().values, index=dataframe.index)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# basic plot\n",
    "s.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Backtesting\n",
    "\n",
    "The goal of time series forecasting is to make accurate predictions about the future. The fast and powerful methods that we rely on in machine learning, such as using *train-test splits* and *k-fold cross-validation*, do not work in the case of time series data. This is because they ignore the temporal components inherent in the problem.\n",
    "\n",
    "In time series forecasting, this **evaluation of models on historical data** is called **backtesting**. We will look at three different methods that you can use to backtest your machine learning models on time series problems:\n",
    "\n",
    "1. **Train-Test split** that respects temporal order of observations.\n",
    "2. **Multiple Train-Test splits** that respect temporal order of observations.\n",
    "3. **Walk-Forward Validation** where a model may be updated each time step new data is received."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# proportion of data used for training\n",
    "split_threshold = 0.9"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Train-Test Split"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate a train-test split of a time series dataset\n",
    "X = s.values\n",
    "train_size = int(len(X) * split_threshold)\n",
    "train, test = X[0:train_size], X[train_size:len(X)]\n",
    "test_size = len(test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Observations: %d' % (len(X)))\n",
    "print('Training Observations: %d' % (train_size))\n",
    "print('Testing Observations: %d' % (test_size))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot train-test split of time series data\n",
    "plt.plot(train);\n",
    "plt.plot([None for i in train] + [x for x in test], color='r');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multiple Train-Test Splits\n",
    "\n",
    "We can repeat the process of splitting the time series into train and test sets multiple times. This will require **multiple models to be trained and evaluated**, but this additional computational expense will provide a more **robust estimate of the expected performance** of the chosen method and configuration on unseen data. We could do this manually by repeating the train-split tests with different split points.\n",
    "\n",
    "The `scikit-learn` library provides a time series \"cross-validator\" in the ```TimeSeriesSplit``` module. It provides train/test indices to split time series data samples that are observed at fixed time intervals, in train/test sets. Note that unlike standard cross-validation methods, **successive training sets are supersets** of those that come before them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate repeated train-test splits of time series data\n",
    "X = s.values\n",
    "splits = TimeSeriesSplit(n_splits=3)\n",
    "index = 1\n",
    "plt.figure(1, figsize=(15,9))\n",
    "\n",
    "for train_index, test_index in splits.split(X):\n",
    "    train = X[train_index]\n",
    "    test = X[test_index]\n",
    "\n",
    "    print('Observations: %d' % (len(train) + len(test)))\n",
    "    print('Training Observations: %d' % (len(train)))\n",
    "    print('Testing Observations: %d\\n-----' % (len(test)))\n",
    "\n",
    "    plt.subplot(310 + index)\n",
    "    plt.plot(train)\n",
    "    plt.plot([None for i in train] + [x for x in test], color='r')\n",
    "    index += 1\n",
    "plt.show() "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using multiple train-test splits will result in more models being trained, and in turn, a\n",
    "more accurate estimate of the performance of the models on unseen data. A **limitation of the\n",
    "train-test split approach** is that the **trained models remain fixed** as they are evaluated on each\n",
    "evaluation in the test set. This may not be realistic as **models can be retrained** as new daily or\n",
    "monthly **observations are made available**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Walk Forward Validation\n",
    "\n",
    "In practice, we very likely will retrain our model as new data becomes available. This would\n",
    "give the model the best opportunity to make good forecasts at each time step.\n",
    "\n",
    "There are few decisions to make:\n",
    "\n",
    "1. **Minimum Number of Observations**. First, we must select the minimum number of observations required to train the model. This may be thought of as the window width if a sliding window is used (see next point).\n",
    "2. **Sliding or Expanding Window**. Next, we need to decide whether the model will be trained on all data it has available or only on the most recent observations. This determines whether a sliding or expanding window will be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot train-test split of time series data\n",
    "plt.plot(train);\n",
    "plt.plot([None for i in train] + [x for x in test], color='r');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# walk forward validation: the train set expanding each time step and the test set fixed at one time step ahead\n",
    "X = s[-100:].values\n",
    "n_train = 80\n",
    "n_records = len(X)\n",
    "\n",
    "for i in range(n_train, n_records):\n",
    "    train, test = X[0:i], X[i:i+1]\n",
    "    print('train=%d, test=%d' % (len(train), len(test)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Forecasting Performance Measures\n",
    "\n",
    "Time series prediction performance measures provide a summary of the skill and capability of the forecast model that made the predictions. There are many different performance measures to choose from. Here we will take a look at:\n",
    "\n",
    "- Time series forecast error calculations that have the same units as the expected outcomes such as **mean absolute error**.\n",
    "- Widely used error calculations that punish large errors, such as **mean squared error** and **root mean squared error**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expected = [0.0, 0.5, 0.0, 0.5, 0.0]\n",
    "predictions = [0.2, 0.4, 0.1, 0.6, 0.2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_expected = pd.Series(expected)\n",
    "s_predictions = pd.Series(predictions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s_expected.plot(color='b');\n",
    "s_predictions.plot(color='r');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate forecast error\n",
    "forecast_errors = [expected[i]-predictions[i] for i in range(len(expected))]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Forecast Errors: %s' % forecast_errors)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mean Forecast Error (or Forecast Bias)\n",
    "\n",
    "Mean forecast error is calculated as the average of the forecast error values.\n",
    "\n",
    "Forecast errors can be positive and negative. This means that when the average of these values is calculated, an ideal mean forecast error would be zero. The forecast error can be calculated directly as the mean of the forecast values. The units of the forecast bias are the same as the units of the predictions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sum(forecast_errors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(expected)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate mean forecast error\n",
    "bias = sum(forecast_errors) / len(expected)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Bias: %f' % bias)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Mean Squared Error\n",
    "\n",
    "The mean squared error, or MSE, is calculated as the average of the squared forecast error values. Squaring the forecast error values forces them to be positive; it also has the effect of putting more weight on large errors.\n",
    "\n",
    "The error values are in squared units of the predicted values. A mean squared error of zero indicates perfect skill, or no error."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate mean squared error\n",
    "mse = mean_squared_error(expected, predictions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('MSE: %f' % mse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Root Mean Squared Error"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The mean squared error described above is in the squared units of the predictions. It can be transformed back into the original units of the predictions by taking the square root of the mean squared error score.\n",
    "\n",
    "This can be calculated by using the `sqrt()` math function on the mean squared error calculated using the `mean_squared_error()` scikit-learn function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from math import sqrt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate root mean squared error\n",
    "rmse = sqrt(mse)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('RMSE: %f' % rmse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The RMSE error values are in the same units as the predictions. As with the mean squared error, an RMSE of zero indicates no error."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Persistence Model for Forecasting\n",
    "\n",
    "Establishing a **baseline** is essential on any time series forecasting problem. A baseline in performance gives you an idea of how well all other models will actually perform on your problem. Here we will develop a persistence forecast that we can use later as a baseline level of performance on this time series dataset.\n",
    "\n",
    "Three properties of a good technique for making a baseline forecast are: **simple**, **fast**, and **repeatable**.\n",
    "\n",
    "The persistence algorithm uses the value at the current time step (t) to predict the expected outcome at the next time step (t+1).\n",
    "\n",
    "The following steps are required:\n",
    "\n",
    "1. Transform the univariate dataset into a supervised learning problem.\n",
    "2. Establish the train and test datasets for the test harness.\n",
    "3. Define the persistence model.\n",
    "4. Make a forecast and establish a baseline performance.\n",
    "5. Review the complete example and plot the output."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 1: Define the Supervised Learning Problem"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create lagged dataset\n",
    "values = pd.DataFrame(s.values)\n",
    "\n",
    "# YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.info()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 2: Train and Test Sets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# split into train and test sets\n",
    "X = df.values\n",
    "\n",
    "# YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 3: Persistence Algorithm"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# persistence model\n",
    "def model_persistence(x):\n",
    "    # YOUR CODE HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 4: Make and Evaluate Forecast"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# walk-forward validation\n",
    "predictions = []\n",
    "\n",
    "# YOUR CODE HERE\n",
    "\n",
    "rmse = np.sqrt(mean_squared_error(test_y, predictions))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "len(predictions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Test RMSE: %.3f' % rmse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The error values of the **root mean squared error** (or **RMSE**) are in the same units as the predictions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# plot predictions and expected results on the test data\n",
    "plt.plot(train_y[-50:])\n",
    "plt.plot([None for i in train_y[-50:]] + [x for x in test_y], label=\"Expected\", color='green')\n",
    "plt.plot([None for i in train_y[-50:]] + [x for x in predictions], label=\"Predicted\", color='red')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize Residual Forecast Errors\n",
    "\n",
    "**Forecast errors** on a time series forecasting problem are called **residual errors** or **residuals**. A residual error is calculated as follows:\n",
    "\n",
    "    residual error = expected - forecast\n",
    "\n",
    "Careful exploration of residual errors can tell you a lot about your forecast model and even suggest improvements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate residuals from the above persistence model\n",
    "residuals = [test_y[i]-predictions[i] for i in range(len(predictions))]\n",
    "residuals = pd.DataFrame(residuals)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Residual Line Plot\n",
    "\n",
    "The first plot is to look at the residual forecast errors over time as a line plot. We would expect the plot to be random around the value of 0 and not show any trend or cyclic structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot residuals\n",
    "residuals.plot(legend=False);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Residual Summary Statistics\n",
    "\n",
    "We can calculate summary statistics on the residual errors. Primarily, we are interested in\n",
    "the mean value of the residual errors. A **value close to zero suggests no bias in the forecasts**,\n",
    "whereas positive and negative values suggest a positive or negative bias in the forecasts made.\n",
    "It is useful to know about a bias in the forecasts as it can be directly corrected in forecasts prior\n",
    "to their use or evaluation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "residuals.describe().T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Residual Histogram and Density Plots"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plots can be used to better understand the distribution of errors beyond summary statistics.\n",
    "We would expect the **forecast errors to be normally distributed around a zero mean**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# histograms plot\n",
    "residuals.hist();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the plot showed a **distribution that was distinctly non-Gaussian**, it would suggest that assumptions made by the modeling\n",
    "process were perhaps incorrect and that a different modeling method may be required. A large skew may suggest the opportunity for **performing a transform to the data prior to modeling**, such as taking the log or square root."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# density plot\n",
    "residuals.plot(kind='kde', legend=False);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Residual Q-Q Plot\n",
    "\n",
    "A Q-Q plot, or **quantile plot**, compares two distributions and can be used to see how similar or different they happen to be. The Q-Q plot can be used to **quickly check the normality of the distribution of residual errors**. The values are ordered and compared to an idealized Gaussian distribution. The comparison is shown as a scatter plot (theoretical on the x-axis and observed on the y-axis) where a match between the two distributions is shown as a diagonal line from the bottom left to the top-right of the plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qqplot(residuals, line='r');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Residual Autocorrelation Plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We would **not expect there to be any correlation between the residuals**. This would be shown by autocorrelation scores being\n",
    "below the threshold of significance (dashed and dotted horizontal lines on the plot). A significant autocorrelation in the residual plot suggests that the model could be doing a better job of incorporating the relationship between observations and lagged observations, called *autoregression*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# autocorrelation plot of residuals\n",
    "plot_acf(residuals, lags=30);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reframe Time Series Forecasting Problems\n",
    "\n",
    "There are many ways to reframe the forecast problem that can both **simplify the prediction problem** and potentially\n",
    "expose more or **different information** to be modeled. A reframing can ultimately result in better and/or more robust forecasts."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Regression Framings\n",
    "\n",
    "Most time series prediction problems are regression problems, requiring the prediction of a real-valued output. This prediction problem could be re-phrased as an **alternate regression problem**, e.g. forecast the change compared to the previous month or forecast the average value for the next 6 months.\n",
    "\n",
    "Here are five different ways that this prediction problem could be re-phrased as an alternate regression problem:\n",
    "- Forecast the change in the minimum temperature compared to the previous day.\n",
    "- Forecast the minimum temperature relative to the average from the past 14 days.\n",
    "- Forecast the minimum temperature relative to the average the same month last year.\n",
    "- Forecast the minimum temperature rounded to the nearest 5 degrees Celsius.\n",
    "- Forecast the average minimum temperature for the next 7 days."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Classification Framings\n",
    "\n",
    "Classification involves predicting categorical or label outputs (such as *high*, *medium*, and *low*). For example, we can forecast whether a change in minimum value will be small or large, or whether the new value will be lower or higher than the current value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create lagged dataset\n",
    "values = pd.DataFrame(s.values)\n",
    "df = pd.concat([values.shift(1), values], axis=1)\n",
    "df.columns = ['t', 't+1']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_discrete(row):\n",
    "    if row['t+1'] < 2:\n",
    "        return 'low'\n",
    "    elif row['t+1'] > 5:\n",
    "        return 'high'\n",
    "    else:\n",
    "        return 'medium'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# apply the above function to reassign t+1 values\n",
    "df['t+1'] = df.apply(lambda row: make_discrete(row), axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Randomly sample 10 elements from the dataframe\n",
    "df.sample(n=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time Horizon Framings\n",
    "\n",
    "* Forecast the minimum value for the next 3 months.\n",
    "* Forecast the maximum value relative to the average from the past 12 months.\n",
    "* Forecast the month in the next year that will have the lowest value.\n",
    "* ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create lagged dataset\n",
    "values = pd.DataFrame(s.values)\n",
    "df = pd.concat([values.shift(1), values, values.shift(-1), values.shift(-2)], axis=1)\n",
    "df.columns = ['t', 't+1', 't+2', 't+3']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now define the problem as: given the interest rate from the month before, forecast the value for the next 3 months. This is known as **multi-step time series forecasting**."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
