{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Time Series Forecasting\n",
    "\n",
    "Time Series Forecasting differ from more traditional classification and regression predictive modeling problems. The temporal structure adds an order to the observations. This imposed order means that important assumptions about the consistency of these observations must be handled specifically.\n",
    "\n",
    "The goal of Time Series Forecasting is to make accurate predictions about the future. In this set of Jupyter notebooks, we will focus on how to make predictions on **univariate time series problems** using the standard tools in the **Python data science ecosystem**. [Pandas](http://pandas.pydata.org/) library in Python provides excellent, built-in support for time series data, while [Statsmodels](http://statsmodels.sourceforge.net/) is a Python module that allows users to explore data, estimate statistical models, and perform statistical tests. \n",
    "\n",
    "For learning Time Series Forecasting with Python, we recommend an excellent book *Introduction to Time Forecasting with Python* (2017) by Jason Brownlee. Several examples and techniques in this notebook are presented and explained in more details in that book.\n",
    "\n",
    "## Part I: Data Wrangling and Exploring\n",
    "\n",
    "#### Load Libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# execute notebook with library imports\n",
    "%run libraries.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# check the versions of key python libraries\n",
    "print('pandas: %s' % pd.__version__)\n",
    "print('numpy: %s' % np.__version__)\n",
    "print('statsmodels: %s' % statsmodels.__version__)\n",
    "print('sklearn: %s' % sklearn.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Display settings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# adjust display settings\n",
    "%matplotlib inline\n",
    "plt.rc('figure', figsize=(18, 3))\n",
    "plt.rcParams['figure.facecolor'] = 'w'\n",
    "pd.set_option('display.float_format', lambda x: '%.2f' % x)\n",
    "pd.options.display.max_rows = 30"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Left-align tables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%html\n",
    "<style>\n",
    "table {float:left}\n",
    "</style>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load Original Data\n",
    "\n",
    "Download the Effective Federal Funds Rate dataset from [FRED Economic Data website](https://fred.stlouisfed.org/series/FEDFUNDS) and put it into directory `/data`. The federal funds rate is the interest rate at which depository institutions trade federal funds with each other overnight.\n",
    "\n",
    "We will focus on forecasting the interest rates up to the 2008 financial crisis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# CSV data\n",
    "original_input_file = 'data/FEDFUNDS.csv'\n",
    "csv_file = 'data/fed.csv'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# focus on the period up to 2007\n",
    "end_year = '2007'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load dataset\n",
    "dataframe = pd.read_csv(original_input_file,\n",
    "                        index_col='DATE',\n",
    "                        parse_dates=True, \n",
    "                        date_parser=lambda date: pd.datetime.strptime(date, '%Y-%m-%d'),\n",
    "                       )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# display the first few lines of a time series\n",
    "dataframe.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# display the last few lines of a time series\n",
    "dataframe.tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# the index is in datetime format, which is fine\n",
    "dataframe.index[:5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# however, this doesn't look good yet\n",
    "dataframe.head().values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# unstacking the data might help...\n",
    "dataframe.unstack().head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# and it does\n",
    "dataframe.unstack().head().values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# sort data by DateTime index, delete index name\n",
    "dataframe.sort_index(inplace=True)\n",
    "dataframe.index.name = None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# assign a shorter column name\n",
    "dataframe.columns = ['fed']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataframe.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# select only data up to this year\n",
    "dataframe = dataframe.loc[:end_year]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataframe.tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create CSV data file (so that we don't have to repeat all this again in other notebooks)\n",
    "dataframe.to_csv(csv_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load and Explore Time Series Data\n",
    "\n",
    "We will load the data that we have just prepared."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load dataset\n",
    "dataframe = pd.read_csv(csv_file,\n",
    "                        index_col=0,\n",
    "                        dtype={'fed': np.float32},\n",
    "                        parse_dates=True, \n",
    "                        date_parser=lambda date: pd.datetime.strptime(date, '%Y-%m-%d'),\n",
    "                       )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create Time Series\n",
    "\n",
    "Pandas represents time series datasets as a Series. A Series is a one-dimensional array with a time label for each row."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a time series\n",
    "s = pd.Series(dataframe.unstack().values, index=dataframe.index)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# basic plot\n",
    "s.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Explore Time Series Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# display first few lines of a time series\n",
    "s.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# display last few lines of a time series\n",
    "s.tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# there is more than one way to do it\n",
    "s.index[:5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# summarize the dimensions of a time series\n",
    "s.size"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate descriptive statistics\n",
    "s.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Querying by date-time index"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# query a dataset using a date-time index\n",
    "s['1998':].head(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# query a dataset using a date-time index\n",
    "s[:'1999-10'].tail(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# query a dataset using a date-time index\n",
    "s['1981-06':'1981-12']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot a slice of time series\n",
    "s['1978-06':'1984-06'].plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic Feature Engineering\n",
    "\n",
    "Time Series data must be re-framed as a supervised learning dataset before we can start using\n",
    "machine learning algorithms. There is no concept of input and output features in time series.\n",
    "Instead, we must choose the variable to be predicted and use feature engineering to construct\n",
    "all of the inputs that will be used to make predictions for future time steps.\n",
    "\n",
    "A time series dataset must be transformed to be modeled as a supervised learning problem. We need to transform something that looks like:\n",
    "\n",
    "    time 1, value 1\n",
    "    time 2, value 2\n",
    "    time 3, value 3\n",
    "\n",
    "into something that looks like:\n",
    "\n",
    "    input 1, output 1\n",
    "    input 2, output 2\n",
    "    input 3, output 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Date Time Features\n",
    "\n",
    "The simplest features that we can use are features from the date/time of each observation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.DataFrame()\n",
    "df['year'] = [s.index[i].year for i in range(len(s))]\n",
    "df['month'] = [s.index[i].month for i in range(len(s))]\n",
    "df['value'] = s.values.tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Lag Features"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lag features are the classical way that time series forecasting problems are transformed into\n",
    "supervised learning problems. The simplest approach is to predict the value at the next time\n",
    "(t+1) given the value at the current time (t). The supervised learning problem with shifted\n",
    "values looks as follows:\n",
    "\n",
    "    Value(t), Value(t+1)\n",
    "    Value(t), Value(t+1)\n",
    "    Value(t), Value(t+1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values = pd.DataFrame(s.values)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values = pd.DataFrame(s.values)\n",
    "df = pd.concat([values.shift(1), values], axis=1)\n",
    "df.columns = ['t', 't+1']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.head(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.tail()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Window Features\n",
    "\n",
    "The addition of lag features is called the sliding window method, in this case with a window width of 1. It is as\n",
    "though we are sliding our focus along the time series for each observation with an interest in only what is within the window width. We can expand the window width and include more lagged features."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values = pd.DataFrame(s.values)\n",
    "df = pd.concat([values.shift(3), values.shift(2), values.shift(1), values], axis=1)\n",
    "df.columns = ['t-2', 't-1', 't', 't+1']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.tail()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Rolling Window Statistics\n",
    "\n",
    "A step beyond adding raw lagged values is to add a summary of the values at previous time steps. We can calculate summary statistics across the values in the sliding window and include these as features in our dataset. Perhaps the most useful is the mean of the previous values, also called the rolling mean."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values = pd.DataFrame(s.values)\n",
    "shifted = values.shift(1)\n",
    "window = shifted.rolling(window=2)\n",
    "means = window.mean()\n",
    "\n",
    "df = pd.concat([means, values], axis=1)\n",
    "df.columns = ['mean(t-1,t)', 't+1']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.iloc[100:150].plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are more statistics we can calculate and even different mathematical ways of calculating\n",
    "the definition of the **window**. Below is another example that shows a window width of 3 and a\n",
    "dataset comprised of more summary statistics, specifically the minimum, mean, and maximum\n",
    "value in the window."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values = pd.DataFrame(s.values)\n",
    "width = 3\n",
    "shifted = values.shift(width - 1)\n",
    "window = shifted.rolling(window=width)\n",
    "\n",
    "df = pd.concat([window.min(), window.mean(), window.max(), values], axis=1)\n",
    "df.columns = ['min', 'mean', 'max', 't+1']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df[['min', 'mean', 'max']].iloc[100:150].plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Expanding Window Statistics\n",
    "\n",
    "Another type of window that may be useful includes all previous data in the series. This is called an expanding window and can help with keeping track of the bounds of observable data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values = pd.DataFrame(s.values)\n",
    "window = values.expanding()\n",
    "\n",
    "df = pd.concat([window.min(), window.mean(), window.max(), values], axis=1)\n",
    "df.columns = ['min', 'mean', 'max', 't+1']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df[['min', 'mean', 'max']].iloc[100:150].plot();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Visualization\n",
    "\n",
    "Time series lends itself naturally to visualization. Line plots of observations over time are popular, but there is a suite of other plots that you can use to learn more about your problem. The more you learn about your data, the more likely you are to develop a better forecasting model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Line Plot\n",
    "\n",
    "Perhaps the most popular visualization for time series is the line plot. In this plot, time is shown on the x-axis with observation values along the y-axis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Commands which take color arguments can use several formats to specify the colors. For the basic built-in colors, you can use a single letter. \n",
    "\n",
    "|   | color   |    |      \n",
    "|:--|---------|----|     \n",
    "| b | blue    | -  |     \n",
    "| g | green   | -- |     \n",
    "| r | red     | -. |     \n",
    "| c | cyan    | :  |     \n",
    "| m | magenta | .  |     \n",
    "| y | yellow  | ,  |     \n",
    "| k | black   | o  |     \n",
    "| w | white   | x  |     "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot(color='g');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Several format string characters are accepted to control the line style or marker.\n",
    "\n",
    "|    | style               |\n",
    "|----|:--------------------|\n",
    "| -  | solid line style    |\n",
    "| -- | dash-dot line style |\n",
    "| -. | dashed line style   |\n",
    "| :  | dotted line style   |\n",
    "| .  | point marker        |\n",
    "| ,  | pixel marker        |\n",
    "| o  | circle marker       |\n",
    "| *  | star marker         |\n",
    "| x  | x marker            |\n",
    "| +  | plus marker         |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot(style='k-.');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Histogram and Density Plots\n",
    "\n",
    "Another important visualization is of the distribution of observations themselves. This means a plot of the values without the temporal ordering. Some linear time series forecasting methods assume a well-behaved distribution of observations (i.e. a bell curve or normal distribution). This can be explicitly checked using tools like statistical hypothesis tests. But plots can provide a useful first check of the distribution of observations both on raw observations and after any type of data transform has been performed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.hist();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot(kind='kde');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Box and Whisker Plots\n",
    "\n",
    "Histograms and density plots provide insight into the distribution of all observations, but we\n",
    "may be interested in the distribution of values by time interval. Another type of plot that is\n",
    "useful to summarize the distribution of observations is the box and whisker plot. This plot\n",
    "draws a box around the 25th and 75th percentiles of the data that captures the middle 50% of\n",
    "observations. A line is drawn at the 50th percentile (the median) and whiskers are drawn above\n",
    "and below the box to summarize the general extents of the observations. Dots are drawn for\n",
    "outliers outside the whiskers or extents of the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "years, index = [], []\n",
    "for name, gr in s.groupby(Grouper(freq='A')):\n",
    "    years.append(gr.values)\n",
    "    index.append(name.year)\n",
    "df = pd.DataFrame(years, index=index, columns=range(1,13))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.tail(10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.head(10).T.boxplot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comparing box and whisker plots by consistent intervals is a useful tool. Within an interval,\n",
    "it can help to spot outliers (dots above or below the whiskers). Across intervals, in this case\n",
    "years, we can look for multiple year trends, seasonality, and other structural information that\n",
    "could be modeled."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df.tail(10).T.boxplot();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s['1998':'1999'].plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Heat Maps\n",
    "\n",
    "A matrix of numbers can be plotted as a surface, where the values in each cell of the matrix are\n",
    "assigned a unique color. This is called a heatmap, as larger values can be drawn with warmer\n",
    "colors (yellows and reds) and smaller values can be drawn with cooler colors (blues and greens).\n",
    "Like the box and whisker plots, we can compare observations between intervals using a heat\n",
    "map."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.matshow(df.T, interpolation=None, aspect='auto');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.heatmap(df.T);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s['1990':'1999'].plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Lag Scatter Plots\n",
    "\n",
    "Time series modeling assumes a relationship between an observation and the previous observation.\n",
    "Previous observations in a time series are called lags, with the observation at the previous time\n",
    "step called lag=1, the observation at two time steps ago lag=2, and so on. A useful type of plot\n",
    "to explore the relationship between each observation and a lag of that observation is called the\n",
    "scatter plot. Pandas has a built-in function for exactly this called the lag plot. It plots the observation at time t on the x-axis and the lag=1 observation (t-1) on the y-axis.\n",
    "\n",
    "* If the points cluster along a diagonal line from the bottom-left to the top-right of the plot,\n",
    "it suggests a positive correlation relationship.\n",
    "* If the points cluster along a diagonal line from the top-left to the bottom-right, it suggests\n",
    "a negative correlation relationship.\n",
    "* Either relationship is good as they can be modeled.\n",
    "\n",
    "More points tighter in to the diagonal line suggests a stronger relationship and more spread\n",
    "from the line suggests a weaker relationship. A ball in the middle or a spread across the plot\n",
    "suggests a weak or no relationship."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a lag scatter plot\n",
    "pd.plotting.lag_plot(s);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create multiple lag scatter plots\n",
    "values = pd.DataFrame(s.values)\n",
    "lags = 8\n",
    "columns = [values]\n",
    "\n",
    "for i in range(1,(lags + 1)):\n",
    "    columns.append(values.shift(i))\n",
    "\n",
    "df = pd.concat(columns, axis=1)\n",
    "columns = ['t+1']\n",
    "\n",
    "for i in range(1,(lags + 1)):\n",
    "    columns.append('t-' + str(i))\n",
    "df.columns = columns\n",
    "\n",
    "plt.figure(1, figsize=(15,9))\n",
    "for i in range(1,(lags + 1)):\n",
    "    ax = plt.subplot(240 + i)\n",
    "    ax.set_title('t+1 vs t-' + str(i))\n",
    "    plt.scatter(x=df['t+1'].values, y=df['t-'+str(i)].values)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Autocorrelation Plots\n",
    "\n",
    "We can quantify the strength and type of relationship between observations and their lags. In statistics, this is called correlation, and when calculated against lag values in time series, it is called autocorrelation (self-correlation). A correlation value calculated between two groups of numbers, such as observations and their lag=1 values, results in a number between -1 and 1. The sign of this number indicates a negative or positive correlation respectively. A value close to zero suggests a weak correlation, whereas a value closer to -1 or 1 indicates a strong correlation.\n",
    "\n",
    "Correlation values, called correlation coeficients, can be calculated for each observation and different lag values. Once calculated, a plot can be created to help better understand how this relationship changes over the lag. This type of plot is called an **autocorrelation plot**.\n",
    "\n",
    "The Statsmodels library provides a version of the autocorrelation plot as a line plot that plots lags on the horizontal and the correlations on vertical axis:\n",
    "- **Autocorrelation Function (ACF)**: It is a measure of the correlation between the time series with their lagged version.\n",
    "- **Partial Autocorrelation Function (PACF)**: It measures the correlation between the time series with their lagged version (like ACF), but eliminating the variations already explained by the intervening comparisons."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Autocorrelation plot from pandas\n",
    "ax = autocorrelation_plot(s[-200:])  # pass subseries of the original series\n",
    "ax.set_xlim([0, 50])                 # limit x-axis to make it more readable"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Autocorrelation Function (ACF)\n",
    "plot_acf(s, lags=30);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Autocorrelation represents the degree of similarity between a given time series and a lagged version of itself over successive time intervals.\n",
    "- Autocorrelation measures the relationship between a variable's current value and its past values.\n",
    "- An autocorrelation of +1 represents a perfect positive correlation, while an autocorrelation of negative 1 represents a perfect negative correlation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Partial Autocorrelation Function (PACF)\n",
    "plot_pacf(s, lags=10);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These two functions may help us to determine parameters *p* and *q* in ARIMA model."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Resampling data\n",
    "\n",
    "Resampling involves changing the frequency of your time series observations. Two types of\n",
    "resampling are:\n",
    "- **Upsampling**: increase the frequency of the samples,\n",
    "- **Downsampling**: decrease the frequency of the samples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are perhaps two main reasons why you may be interested in resampling your time series data:\n",
    "- **Problem Framing**: Resampling may be required if your data is available at the same frequency that you want to make predictions.\n",
    "- **Feature Engineering**: Resampling can also be used to provide additional structure or insight into the learning problem for supervised learning models."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "resample = s.resample('Q')\n",
    "quarterly_mean = resample.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "quarterly_mean.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "quarterly_mean.tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot together with the original data\n",
    "s.plot()\n",
    "quarterly_mean.plot(color='red', style='.')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# zoom plot\n",
    "s.tail(90).plot(style='.')\n",
    "quarterly_mean.tail(30).plot(color='red', style='.')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Power Transforms\n",
    "\n",
    "Data transforms are intended to remove noise and improve the signal in time series forecasting. It can be very difficult to select a good, or even best, transform for a given prediction problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Log transform\n",
    "\n",
    "Log transforms are popular with time series data as they are effective at removing exponential\n",
    "variance. It is important to note that this operation assumes values are positive and non-zero.\n",
    "It is common to transform observations by adding a fixed constant to ensure all input values\n",
    "meet this requirement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def log_transform(data):\n",
    "    return (np.log(data+1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def log_inverse_transform(data):\n",
    "    return np.exp(data) - 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Time series with an exponential distribution (note that this is not the case here!) can be made linear by taking the logarithm of the values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "log_transform(s).plot(color='orange');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot()\n",
    "log_transform(s).plot(color='orange');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s1 = log_transform(s);\n",
    "s2 = log_inverse_transform(s1);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s1.plot(color='orange');\n",
    "s2.plot(color='green');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.hist(s);\n",
    "plt.hist(s1);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Moving Average Smoothing\n",
    "\n",
    "Moving average smoothing is a naive and effective technique in time series forecasting. It can be used for **data preparation**, **feature engineering**, and even directly for **making predictions**.\n",
    "\n",
    "Smoothing is a technique applied to time series to **remove the fine-grained variation** between time steps. The hope of smoothing is to **remove noise** and better expose the signal of the underlying causal processes. The moving average can be used as a **source of new information** when modeling a time series forecast as a supervised learning problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# tail-rolling average transform\n",
    "rolling = s.rolling(window=3)\n",
    "rolling_mean = rolling.mean()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rolling_mean.head(10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot original and transformed dataset\n",
    "s[-100:].plot();\n",
    "rolling_mean[-100:].plot(color='red');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Moving Average as Prediction\n",
    "\n",
    "The moving average value can also be used directly to make predictions. It is a naive model\n",
    "and assumes that the trend and seasonality components of the time series have already been\n",
    "removed or adjusted for. The moving average model for predictions can easily be used in a\n",
    "walk-forward manner. As new observations are made available (e.g. monthly), the model can be\n",
    "updated and a prediction made for the next month."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# prepare problem\n",
    "X = s.values\n",
    "window = 3\n",
    "history = [X[i] for i in range(window)]\n",
    "test = [X[i] for i in range(window, len(X))]\n",
    "predictions = []\n",
    "\n",
    "# walk forward over time steps in test\n",
    "for t in range(len(test)):\n",
    "    length = len(history)\n",
    "    yhat = np.mean([history[i] for i in range(length-window,length)])\n",
    "    obs = test[t]\n",
    "    predictions.append(yhat)\n",
    "    history.append(obs)\n",
    "    #print('predicted=%f, expected=%f' % (yhat, obs))\n",
    "rmse = np.sqrt(mean_squared_error(test, predictions))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('RMSE: %.3f' % rmse)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# plot\n",
    "plt.plot(test);\n",
    "plt.plot(predictions, color='red');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# zoom plot\n",
    "plt.plot([None for i in test[:-100]] + [x for x in test[-100:]], label=\"Expected\")\n",
    "plt.plot([None for i in predictions[:-100]] + [x for x in predictions[-100:]], label=\"Predicted\", color=\"red\")\n",
    "plt.legend(loc='lower right')\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
