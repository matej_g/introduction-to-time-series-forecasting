{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Time Series Forecasting\n",
    "\n",
    "Time Series Forecasting differ from more traditional classification and regression predictive modeling problems. The temporal structure adds an order to the observations. This imposed order means that important assumptions about the consistency of these observations must be handled specifically.\n",
    "\n",
    "The goal of Time Series Forecasting is to make accurate predictions about the future. In this set of Jupyter notebooks, we will focus on how to make predictions on **univariate time series problems** using the standard tools in the **Python data science ecosystem**. [Pandas](http://pandas.pydata.org/) library in Python provides excellent, built-in support for time series data, while [Statsmodels](http://statsmodels.sourceforge.net/) is a Python module that allows users to explore data, estimate statistical models, and perform statistical tests. \n",
    "\n",
    "For learning Time Series Forecasting with Python, we recommend an excellent book *Introduction to Time Forecasting with Python* (2017) by Jason Brownlee. Several examples and techniques in this notebook are presented and explained in more details in that book.\n",
    "\n",
    "## Part II: Temporal Data Structure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# execute notebook with library imports\n",
    "%run libraries.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# check the versions of key python libraries\n",
    "print('pandas: %s' % pd.__version__)\n",
    "print('numpy: %s' % np.__version__)\n",
    "print('statsmodels: %s' % statsmodels.__version__)\n",
    "print('sklearn: %s' % sklearn.__version__)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# adjust display settings\n",
    "%matplotlib inline\n",
    "plt.rc('figure', figsize=(18, 3))\n",
    "plt.rcParams['figure.facecolor'] = 'w'\n",
    "pd.set_option('display.float_format', lambda x: '%.2f' % x)\n",
    "pd.options.display.max_rows = 30"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Is your Time Series White Noise?\n",
    "\n",
    "White noise is an important concept in time series forecasting. If a time series is white noise, it\n",
    "is a sequence of random numbers and cannot be predicted.\n",
    "\n",
    "Your time series is **not** white noise if any of the following conditions are true:\n",
    "* Does your series have a non-zero mean?\n",
    "* Does the variance change over time?\n",
    "* Do values correlate with lag values?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from random import gauss\n",
    "from random import seed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# seed random number generator\n",
    "seed(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create white noise series\n",
    "series = [gauss(0.0, 1.0) for i in range(1000)]\n",
    "series = pd.Series(series)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some tools that you can use to check if your time series is white noise are:\n",
    "\n",
    "- **Create a line plot.** Check for gross features like a changing mean, variance, or obvious\n",
    "relationship between lagged variables.\n",
    "- **Calculate summary statistics.** Check the mean and variance of the whole series against\n",
    "the mean and variance of meaningful contiguous blocks of values in the series (e.g. days,\n",
    "months, or years).\n",
    "- **Create an autocorrelation plot.** Check for gross correlation between lagged variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# summary stats\n",
    "series.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that the mean is nearly 0.0 and the standard deviation is nearly 1.0. Some variance is expected given the small size of the sample."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# line plot\n",
    "series.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that it does appear that the series is random.\n",
    "\n",
    "We can also create a histogram and confirm the distribution is Gaussian."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# histogram plot\n",
    "series.hist();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Indeed, the histogram shows the tell-tale bell-curve shape.\n",
    "\n",
    "Finally, we can create a correlogram and check for any autocorrelation with lag variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# autocorrelation\n",
    "autocorrelation_plot(series);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The correlogram does not show any obvious autocorrelation pattern. There are some spikes above the 95% and 99% confidence level, but these are a statistical fluke."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Autocorrelation Function (ACF)\n",
    "plot_acf(series, lags=30);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Partial Autocorrelation Function (PACF)\n",
    "plot_pacf(series, lags=10);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a lag scatter plot\n",
    "pd.plotting.lag_plot(series);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create multiple lag scatter plots\n",
    "values = pd.DataFrame(series.values)\n",
    "lags = 8\n",
    "columns = [values]\n",
    "\n",
    "for i in range(1,(lags + 1)):\n",
    "    columns.append(values.shift(i))\n",
    "\n",
    "df = pd.concat(columns, axis=1)\n",
    "columns = ['t+1']\n",
    "\n",
    "for i in range(1,(lags + 1)):\n",
    "    columns.append('t-' + str(i))\n",
    "df.columns = columns\n",
    "\n",
    "plt.figure(1, figsize=(15,9))\n",
    "for i in range(1,(lags + 1)):\n",
    "    ax = plt.subplot(240 + i)\n",
    "    ax.set_title('t+1 vs t-' + str(i))\n",
    "    plt.scatter(x=df['t+1'].values, y=df['t-'+str(i)].values)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# prepare data for correlation matrix heatmap\n",
    "values = pd.DataFrame(series.values)\n",
    "lags = 16\n",
    "columns = [values]\n",
    "\n",
    "for i in range(1,(lags + 1)):\n",
    "    columns.append(values.shift(i))\n",
    "\n",
    "df = pd.concat(columns, axis=1)\n",
    "columns = ['t']\n",
    "\n",
    "for i in range(1,(lags + 1)):\n",
    "    columns.append('t-' + str(i))\n",
    "df.columns = columns\n",
    "\n",
    "df.dropna(inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# correlation matrix heatmap\n",
    "corrmat = df.corr()\n",
    "\n",
    "# set up the matplotlib figure\n",
    "f, ax = plt.subplots(figsize=(7, 5))\n",
    "\n",
    "# draw the heatmap using seaborn\n",
    "sns.heatmap(corrmat, vmax=1.0, square=True)\n",
    "\n",
    "f.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Is Your Time Series a Random Walk?\n",
    "\n",
    "How do you know your time series problem is predictable? This is a difficult question with\n",
    "time series forecasting. There is a tool called a random walk that can help you understand the\n",
    "predictability of your time series forecast problem.\n",
    "\n",
    "Your time series may be a random walk. Some ways to check if your time series is a random\n",
    "walk are as follows:\n",
    "\n",
    "- The time series shows a **strong temporal dependence that decays linearly or in a similar\n",
    "pattern**.\n",
    "- The time series is **non-stationary** and **making it stationary shows no obviously learnable structure in the data**.\n",
    "- The **persistence model provides the best source of reliable predictions**.\n",
    "\n",
    "This last point is key for time series forecasting. Baseline forecasts with the persistence\n",
    "model quickly flesh out whether you can do significantly better. If you can't, you're probably\n",
    "working with a random walk. Many time series are random walks, particularly those of security\n",
    "prices over time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from random import randrange\n",
    "from random import seed\n",
    "from random import random"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# seed random number generator\n",
    "seed(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use the randrange() function to generate a list of 1,000 random integers between 0 and 10."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a random series\n",
    "series = [randrange(10) for i in range(1000)]\n",
    "plt.plot(series);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**This is not a random walk.** It is just a sequence of random numbers also called **white noise**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Random Walk series\n",
    "\n",
    "A random walk is different from a list of random numbers because the next value in the sequence is a modification of the previous value in the sequence. The process used to generate the series forces dependence from one-time step to the next. This dependence provides some consistency from step-to-step rather than the large jumps that a series of independent, random numbers\n",
    "provides."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "seed(1)\n",
    "random_walk = list()\n",
    "random_walk.append(-1 if random() < 0.5 else 1)\n",
    "for i in range(1, 1000):\n",
    "    movement = -1 if random() < 0.5 else 1\n",
    "    value = random_walk[i-1] + movement\n",
    "    random_walk.append(value)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see that it looks very different from our above sequence of random numbers. In fact, the shape and movement looks\n",
    "like a realistic time series for the price of a security on the stock market."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# line plot\n",
    "plt.plot(random_walk);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the way that the random walk is constructed, we would expect a strong autocorrelation with the previous observation and a linear fall off from there with previous lag values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# autocorrelation\n",
    "autocorrelation_plot(random_walk);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We generally see the expected trend, in this case across the first few hundred lag observations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Autocorrelation Function (ACF)\n",
    "plot_acf(random_walk, lags=30);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can make the random walk stationary by taking the first difference. That is replacing each observation as the difference between it and the previous value. Given the way that this random walk was constructed, we would expect this to result in a time series of -1 and 1 values. This is exactly what we see."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "seed(1)\n",
    "random_walk = list()\n",
    "random_walk.append(-1 if random() < 0.5 else 1)\n",
    "for i in range(1, 1000):\n",
    "    movement = -1 if random() < 0.5 else 1\n",
    "    value = random_walk[i-1] + movement\n",
    "    random_walk.append(value)\n",
    "\n",
    "# take difference\n",
    "diff = list()\n",
    "for i in range(1, len(random_walk)):\n",
    "    value = random_walk[i] - random_walk[i - 1]\n",
    "    diff.append(value)\n",
    "# line plot\n",
    "plt.plot(diff);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot of the Correlogram of the Differenced Random Walk. We can see no signicant relationship between the lagged observations, as we would expect from the way the random walk was generated. All correlations are small, close to zero and below the 95% and 99% confidence levels (beyond a few statistical flukes)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# autocorrelation\n",
    "autocorrelation_plot(diff);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a Series from the data in the list\n",
    "random_walk_series = pd.Series(random_walk)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a lag scatter plot\n",
    "pd.plotting.lag_plot(random_walk_series);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create multiple lag scatter plots\n",
    "values = pd.DataFrame(random_walk_series.values)\n",
    "lags = 8\n",
    "columns = [values]\n",
    "\n",
    "for i in range(1,(lags + 1)):\n",
    "    columns.append(values.shift(i))\n",
    "\n",
    "df = pd.concat(columns, axis=1)\n",
    "columns = ['t+1']\n",
    "\n",
    "for i in range(1,(lags + 1)):\n",
    "    columns.append('t-' + str(i))\n",
    "df.columns = columns\n",
    "\n",
    "plt.figure(1, figsize=(15,9))\n",
    "for i in range(1,(lags + 1)):\n",
    "    ax = plt.subplot(240 + i)\n",
    "    ax.set_title('t+1 vs t-' + str(i))\n",
    "    plt.scatter(x=df['t+1'].values, y=df['t-'+str(i)].values)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# prepare data for correlation matrix heatmap\n",
    "values = pd.DataFrame(random_walk_series.values)\n",
    "lags = 16\n",
    "columns = [values]\n",
    "\n",
    "for i in range(1,(lags + 1)):\n",
    "    columns.append(values.shift(i))\n",
    "\n",
    "df = pd.concat(columns, axis=1)\n",
    "columns = ['t']\n",
    "\n",
    "for i in range(1,(lags + 1)):\n",
    "    columns.append('t-' + str(i))\n",
    "df.columns = columns\n",
    "\n",
    "df.dropna(inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# correlation matrix heatmap\n",
    "corrmat = df.corr()\n",
    "\n",
    "# set up the matplotlib figure\n",
    "f, ax = plt.subplots(figsize=(7, 5))\n",
    "\n",
    "# draw the heatmap using seaborn\n",
    "sns.heatmap(corrmat, vmax=1.0, square=True)\n",
    "\n",
    "f.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It looks very much different from the correlation matrix heatmap of the White Noise time series, doesn't it?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Predicting a Random Walk\n",
    "\n",
    "A random walk is unpredictable; it cannot reasonably be predicted. Given the way that the\n",
    "random walk is constructed, we can expect that the best prediction we could make would be to\n",
    "use the observation at the previous time step as what will happen in the next time step. Simply\n",
    "because we know that the next time step will be a function of the prior time step.\n",
    "\n",
    "We can implement this in Python by first splitting the dataset into train and test sets, then\n",
    "using the persistence model to predict the outcome using a rolling forecast method. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# prepare dataset\n",
    "train_size = int(len(random_walk) * 0.66)\n",
    "train, test = random_walk[0:train_size], random_walk[train_size:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once all\n",
    "predictions are collected for the test set, the root mean squared error (RMSE) is calculated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# persistence\n",
    "predictions = list()\n",
    "history = train[-1]\n",
    "for i in range(len(test)):\n",
    "    yhat = history\n",
    "    predictions.append(yhat)\n",
    "    history = test[i]\n",
    "rmse = np.sqrt(mean_squared_error(test, predictions))\n",
    "print('Persistence RMSE: %.3f' % rmse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Time Series Components\n",
    "\n",
    "A useful abstraction for selecting forecasting methods is to break a time series down into systematic and unsystematic components.\n",
    "\n",
    "* **Systematic**: Components of the time series that have consistency or recurrence and can\n",
    "be described and modeled.\n",
    "* **Non-Systematic**: Components of the time series that cannot be directly modeled.\n",
    "\n",
    "A given time series is thought to consist of three systematic components including level,\n",
    "trend, seasonality, and one non-systematic component called noise. These components are\n",
    "defined as follows:\n",
    "\n",
    "* **Level**: The average value in the series.\n",
    "* **Trend**: The increasing or decreasing value in the series.\n",
    "* **Seasonality**: The repeating short-term cycle in the series.\n",
    "* **Noise**: The random variation in the series.\n",
    "\n",
    "A time series where the seasonal component has been removed is called **seasonal stationary**. If a dataset does not have a trend or we successfully remove the trend, the dataset is said to be **trend stationary**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# CSV data\n",
    "csv_file = 'data/fed.csv'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load dataset\n",
    "dataframe = pd.read_csv(csv_file,\n",
    "                        index_col=0,\n",
    "                        parse_dates=True, \n",
    "                        date_parser=lambda date: pd.datetime.strptime(date, '%Y-%m-%d'),\n",
    "                       )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a time series\n",
    "s = pd.Series(dataframe.unstack().values, index=dataframe.index)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# basic plot\n",
    "s.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Automatic Time Series Decomposition\n",
    "\n",
    "You must be careful to be critical when interpreting the result of automatic time series decomposition. The snippet below shows how to decompose a series into trend, seasonal, and residual components assuming an additive model. The result object provides access to the trend and seasonal series as arrays. It also provides access to the residuals, which are the time series\n",
    "after the trend and seasonal components are removed. The original or observed data is also stored."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result = seasonal_decompose(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's view again the original (or observed) data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result.observed.plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our time series dataset may contain a **trend**. A trend is a continued increase or decrease in\n",
    "the series over time. There can be benefit in identifying, modeling, and even removing trend\n",
    "information from your time series dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result.trend.plot(color='r');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Time series datasets can contain a **seasonal component**. This is a cycle that repeats over time,\n",
    "such as monthly or yearly. This repeating cycle may obscure the signal that we wish to model\n",
    "when forecasting, and in turn may provide a strong signal to our predictive models. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result.seasonal.plot(color='g');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Residuals** are the time series after the trend and seasonal components are removed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result.resid.plot(color='k');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stationarity in Time Series Data\n",
    "\n",
    "The observations in a **stationary time series are not dependent on time**. Time series are stationary\n",
    "if they **do not have trend or seasonal effects**. Summary statistics calculated on the time series\n",
    "are consistent over time, like the mean or the variance of the observations. When a time series\n",
    "is stationary, it can be **easier to model**. Statistical modeling methods assume or require the\n",
    "time series to be stationary to be effective."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Check for Stationarity\n",
    "\n",
    "There are many methods to check whether a time series (direct observations, residuals, otherwise) is stationary or non-stationary.\n",
    "\n",
    "* **Look at Plots**: You can review a time series plot of your data and visually check if there are any obvious trends or seasonality.\n",
    "* **Summary Statistics**: You can review the summary statistics for your data for seasons or random partitions and check for obvious or significant differences.\n",
    "* **Statistical Tests**: You can use statistical tests to check if the expectations of stationarity are met or have been violated."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rc('figure', figsize=(18, 9))\n",
    "seasonal_decompose(s).plot();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rc('figure', figsize=(18, 3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Augmented Dickey-Fuller test\n",
    "\n",
    "The Augmented Dickey-Fuller test is a type of statistical test called a unit root test. The intuition behind a unit root test is that it determines how strongly a time series is defined by a trend. It uses an autoregressive model and optimizes an information criterion across multiple different lag values.\n",
    "\n",
    "- **Null Hypothesis (H0)**: If accepted, it suggests the time series has a unit root, meaning it is **non-stationary**. It has some time dependent structure.\n",
    "- **Alternate Hypothesis (H1)**: The null hypothesis is rejected; it suggests the time series does not have a unit root, meaning it is **stationary**. It does not have time-dependent structure.\n",
    "\n",
    "We interpret this result using the p-value from the test. A p-value below a threshold (such as 5% or 1%) suggests we accept the null hypothesis (non-stationary), otherwise a p-value above the threshold suggests we reject the null hypothesis (stationary)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate stationarity test of time series data\n",
    "X = s.values\n",
    "result = adfuller(X)\n",
    "print('ADF Statistic: %f' % result[0])\n",
    "print('p-value: %f' % result[1])\n",
    "print('Critical Values:')\n",
    "for key, value in result[4].items():\n",
    "    print('\\t%s: %.3f' % (key, value))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Rejecting the null hypothesis means that the process has no unit root and that the time series is **stationary**, i.e. it **does not have time-dependent structure**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Trends in Time Series\n",
    "\n",
    "A trend is a long-term increase or decrease in the level of the time series.\n",
    "\n",
    "Identifying and understanding trend information can aid in improving model performance; among the reasons are: **faster modeling**, **problem simplification**, and even **more data** (by using trend information).\n",
    "\n",
    "There are all kinds of trends. Two general classes that we may think about are:\n",
    "- **Deterministic Trends**: These are trends that consistently increase or decrease.\n",
    "- **Stochastic Trends**: These are trends that increase and decrease inconsistently.\n",
    "\n",
    "We can think about trends in terms of their scope of observations.\n",
    "- **Global Trends**: These are trends that apply to the whole time series.\n",
    "- **Local Trends**: These are trends that apply to parts or subsequences of a time series."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Detrend by Differencing\n",
    "\n",
    "The simplest method to detrend a time series is by differencing. A new series is constructed where the value at the current time step is calculated as the difference between the original observation and the observation at the previous time step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = s.values\n",
    "diff = []\n",
    "\n",
    "# YOUR CODE HERE\n",
    "\n",
    "diff = np.array(diff)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.head(6)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diff[:5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a time series\n",
    "differenced = pd.Series(diff, index=dataframe.index[1:])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "differenced.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "differenced.tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(differenced, color='r');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate stationarity test of time series data\n",
    "X = np.array(diff)\n",
    "result = adfuller(X)\n",
    "print('ADF Statistic: %f' % result[0])\n",
    "print('p-value: %f' % result[1])\n",
    "print('Critical Values:')\n",
    "for key, value in result[4].items():\n",
    "    print('\\t%s: %.3f' % (key, value))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Seasonality in Time Series\n",
    "\n",
    "Time series datasets can contain a seasonal component. This is **a cycle that repeats over time**, such as daily, monthly or yearly. This repeating cycle may **obscure the signal** that we wish to model when forecasting, and in turn may provide a strong signal to our predictive models."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Removing Seasonality\n",
    "\n",
    "Understanding the seasonal component in time series can improve the performance of modeling with machine learning. Identifying and **removing the seasonal component** from the time series can result in a clearer relationship between input and output variables.\n",
    "\n",
    "Once seasonality is identified, it can be modeled. The model of seasonality can be removed from the time series:\n",
    "- **Seasonal Adjustment with Differencing**: A simple way to correct for a seasonal component is to use differencing. If there is a seasonal component at the level of one week, then we can remove it on an observation today by subtracting the value from last week.\n",
    "- **Seasonal Adjustment with Modeling**: We can model the seasonal component directly, then subtract it from the observations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.plot();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "original_decomposed = seasonal_decompose(s)\n",
    "original_decomposed.seasonal.plot(color='g');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(differenced, color='r');"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rc('figure', figsize=(18, 9))\n",
    "seasonal_decompose(differenced).plot();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Removing Seasonality\n",
    "\n",
    "Once seasonality is identified, it can be modeled. The model of seasonality can be removed from the time series. This process is called **seasonal adjustment**, or **deseasonalizing**. A time series where the seasonal component has been removed is called **seasonal stationary**. A time series with a clear seasonal component is referred to as non-stationary."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
