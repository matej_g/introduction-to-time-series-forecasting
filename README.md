# Introduction to time series forecasting

## Description

This workshop is primarily aimed at programmers (academics, professionals, 
or students) that know some machine learning and want to learn the basics of 
time series forecasting. Time series forecasting includes making accurate 
predictions about the future and is an important area of machine learning that 
is often neglected.

We will focus on how to make predictions on both univariate and multivariate 
time series problems using standard tools in the Python data science ecosystem.

## Event syllabus

*  data wrangling and exploring time series data
*  time series components
*  evaluation of time series models
*  moving average models and autoregression models
*  AutoRegressive Integrated Moving Average model (ARIMA)
*  multivariate time series forecasting

## Prerequisites

It is recommended that you bring your laptops with you, as we will try to be as 
interactive as possible. Please download and install [Anaconda](https://www.anaconda.com/distribution/) 
(choose Python 3.9).

We will work in [Jupyter Notebook](https://jupyter.org/) environment. Run the
`prerequisites` notebook to check whether you have all libraries installed.
