{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Multivariate Time Series Forecasting with XGBoost\n",
    "\n",
    "Time series is different from more traditional classification and regression predictive modeling problems. The temporal structure adds an order to the observations. This imposed order means that important assumptions about the consistency of those observations needs to be handled specifically.\n",
    "\n",
    "The goal of Time Series Forecasting is to make accurate predictions about the future. In this Jupyter Notebook, we will focus on how to make predictions on **multivariate time series problems** using regression with gradient boosting trees algorithm [XGBoost](https://xgboost.readthedocs.io/en/latest/) and the standard tools in the Python ecosystem.\n",
    "\n",
    "In this notebook, we will supplement the federal funds interest rate data (available at [FRED Economic Data website](https://fred.stlouisfed.org/series/FEDFUNDS)) with other, somewhat related time series data. We will focus on forecasting the federal funds interest rates up to the 2008 financial crisis.\n",
    "\n",
    "For learning more about gradient boosting with Python, we recommend you an excellent book *XGBoost With Python* (2016) by Jason Brownlee."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# execute notebook with library imports\n",
    "%run libraries.ipynb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# check the versions of key python libraries\n",
    "print('pandas: %s' % pd.__version__)\n",
    "print('numpy: %s' % np.__version__)\n",
    "print('statsmodels: %s' % statsmodels.__version__)\n",
    "print('sklearn: %s' % sklearn.__version__)\n",
    "print('xgboost: %s' % xgboost.__version__)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# adjust display settings\n",
    "%matplotlib inline\n",
    "plt.rc('figure', figsize=(18, 3))\n",
    "plt.rcParams['figure.facecolor'] = 'w'\n",
    "pd.set_option('display.float_format', lambda x: '%.2f' % x)\n",
    "pd.options.display.max_rows = 30"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load and Explore Data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# CSV data\n",
    "csv_file = 'data/fed_multivariate.csv'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# choose target time series for forecasting\n",
    "target = 'fed'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# load dataset\n",
    "dataframe = pd.read_csv(csv_file,\n",
    "                        index_col=0,\n",
    "                        parse_dates=True, \n",
    "                        date_parser=lambda date: pd.datetime.strptime(date, '%Y-%m-%d'),\n",
    "                       )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "dataframe.info()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "dataframe.head().T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "dataframe.tail().T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "dataframe.describe().T"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following line plot will look familiar."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# plot target time series for forecasting\n",
    "dataframe[target].plot();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# plot other time series data\n",
    "dataframe.iloc[:,1:].plot();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# plot each column\n",
    "plt.figure(figsize=(18, 9))\n",
    "values = dataframe.values\n",
    "\n",
    "# specify columns to plot\n",
    "columns = list(range(len((list(dataframe)))))\n",
    "\n",
    "i = 1\n",
    "for group in columns:\n",
    "    plt.subplot(len(columns), 1, i)\n",
    "    plt.plot(values[:, group])\n",
    "    plt.title(dataframe.columns[group], y=0.5, loc='right')\n",
    "    i += 1\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Did you notice different scales on y-axis?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data Preparation\n",
    "\n",
    "The first step is to prepare the dataset for the XGBoost. This involves framing the dataset as a supervised learning problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Add next column¶"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def add_next_column(gr, attribute):\n",
    "    \"\"\"Add column with the values of the next month for the given attribute.\n",
    "        :param gr: data frame\n",
    "        :param attribute: target attribute\n",
    "        \n",
    "        :return: data frame with new columns\n",
    "    \"\"\"\n",
    "    new_attribute = \"%s_next\" % attribute\n",
    "    # YOUR CODE HERE\n",
    "    \n",
    "    return gr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_temp = pd.DataFrame()\n",
    "gr = add_next_column(df, target)\n",
    "df_temp = pd.concat([df_temp, gr])\n",
    "df = df_temp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "attr = target\n",
    "attr_next = \"%s_next\" % attr\n",
    "df[[attr, attr_next]].tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df.dropna(inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df[[attr, attr_next]].tail()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df[attr].plot();\n",
    "df[attr_next].plot();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Add lag columns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "lag_months = 12"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def add_lag_column(gr, attribute, lag):\n",
    "    \"\"\"Add column with the values of the <lag> months difference for the given attribute.\n",
    "        :param gr: data frame\n",
    "        :param attribute: target attribute\n",
    "        \n",
    "        :return: data frame with new columns of int64 type\n",
    "    \"\"\"\n",
    "    new_attribute = \"%s_d%s\" % (attribute, lag)\n",
    "    # YOUR CODE HERE\n",
    "    \n",
    "    return gr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df_temp = pd.DataFrame()\n",
    "\n",
    "# get differenced values of the target attribute for each <lag> months\n",
    "for lag in range(1, lag_months+1):\n",
    "    gr = add_lag_column(df, target, lag)\n",
    "    \n",
    "df_temp = pd.concat([df_temp, gr])\n",
    "df = df_temp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df.head().T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df.dropna(inplace=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# move the \"next column\" to the end\n",
    "target_next = \"%s_next\" % target\n",
    "cols = list(df.columns.values)\n",
    "cols.remove(target_next)\n",
    "cols.append(target_next)\n",
    "df = df[cols]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "df.head().T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# correlation matrix heatmap: basic attributes\n",
    "corrmat = df.corr()\n",
    "\n",
    "# set up the matplotlib figure\n",
    "f, ax = plt.subplots(figsize=(10, 7))\n",
    "\n",
    "# draw the heatmap using seaborn\n",
    "sns.heatmap(corrmat, vmax=1.0, square=True)\n",
    "\n",
    "f.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## XGBoost\n",
    "\n",
    "Gradient boosting is one of the most powerful techniques for building predictive models.\n",
    "\n",
    "XGBoost is an algorithm that has recently been dominating applied machine learning and Kaggle competitions for structured or tabular data. XGBoost is an implementation of gradient boosted decision trees designed for speed and performance.\n",
    "\n",
    "XGBoost stands for eXtreme Gradient Boosting. It is an implementation of gradient boosting machines created by Tianqi Chen, now with contributions from many developers. The two reasons to use XGBoost are also the two goals of the project: **execution speed** and **model performance**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Feature importances"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "array = df.values\n",
    "last = df.shape[1] - 1\n",
    "X = array[:,0:last]\n",
    "y = array[:,last]\n",
    "\n",
    "print(\"%d examples used for learning, %d attributes\" % (X.shape[0], X.shape[1]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "model = XGBRegressor(objective='reg:squarederror')\n",
    "model.fit(X, y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# store features with positive scores\n",
    "cols = df.columns\n",
    "positive_scoring_features = {}\n",
    "\n",
    "# print out all attributes above the given threshold sorted by their importance\n",
    "for attribute, score in sorted(zip(cols[:-1], model.feature_importances_), key=lambda x: -x[1]):\n",
    "    if score > 0:\n",
    "        positive_scoring_features[attribute] = score\n",
    "        print(\"%41s\\t%f\" % (attribute, score))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "importance = pd.DataFrame(sorted(zip(cols[:-1], model.feature_importances_), key=lambda x: -x[1]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "dfx = pd.DataFrame(importance)\n",
    "dfx.columns = ['feature', 'fscore']\n",
    "dfx.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "dfx['fscore'] = dfx['fscore'] / dfx['fscore'].sum()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "plt.figure()\n",
    "dfx[::-1].tail(30).plot(kind='barh', x='feature', y='fscore', legend=False, figsize=(6, 12))\n",
    "plt.title('XGBoost Feature Importance')\n",
    "plt.xlabel('relative importance')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Split data\n",
    "\n",
    "First, we must split the prepared dataset into train and test sets. We split the dataset into train and test sets, then split the train and test sets into input and output variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "test_size = 65  # choose the same size as with the univariate models"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# prepare data\n",
    "data = df.values\n",
    "train_size = int(df.shape[0] - test_size)\n",
    "test = df[target_next].tail(test_size).tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "last = data.shape[1] - 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "X_train = data[:train_size, :last]\n",
    "y_train = data[:train_size, last]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "X_train.shape, y_train.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "X_test = data[train_size:, :last]\n",
    "y_test = data[train_size:, last]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# plot train-test split of time series data\n",
    "plt.plot(y_train);\n",
    "plt.plot([None for i in y_train] + [x for x in y_test], color='r');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Persistence Model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# persistence model\n",
    "predictions = df[target].tail(test_size).tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "predictions[:5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "len(predictions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# skill of persistence model\n",
    "rmse = np.sqrt(mean_squared_error(y_test, predictions))\n",
    "print('Test RMSE: %.5f' % rmse)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# plot forecasts against actual outcomes\n",
    "plt.rc('figure', figsize=(18, 3))\n",
    "plt.plot(y_test, label=\"Expected\")\n",
    "plt.plot(predictions, label=\"Predicted\", color='red')\n",
    "plt.legend(loc='upper left')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Walk forward validation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def evaluate_regression_model(data, model, train_size, test):\n",
    "    predictions = []\n",
    "    last = data.shape[1] - 1\n",
    "\n",
    "    # walk forward\n",
    "    for t in range(test_size):\n",
    "        X_train = data[:train_size+t, :last]\n",
    "        y_train = data[:train_size+t, last]\n",
    "        X_test = data[train_size+t:, :last]\n",
    "        y_test = data[train_size+t:, last]\n",
    "        \n",
    "        # YOUR CODE HERE\n",
    "        \n",
    "        #print('predicted=%f, expected=%f' % (yhat, obs))\n",
    " \n",
    "    rmse = np.sqrt(mean_squared_error(test, predictions))\n",
    "    return rmse, predictions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# prepare data\n",
    "data = df.values\n",
    "train_size = int(data.shape[0] - test_size)\n",
    "test = df[target_next].tail(test_size).tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# train model\n",
    "model = XGBRegressor(objective='reg:squarederror')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "rmse, predictions = evaluate_regression_model(data, model, train_size, test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print('RMSE: %.5f' % rmse)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "len(predictions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "plt.plot([None for i in df[target_next].values[:train_size]] + [x for x in df[target_next].tail(test_size).tolist()], label=\"Expected\")\n",
    "plt.plot([None for i in df[target_next].values[:train_size]] + [x for x in predictions], label=\"Predicted\", color=\"red\")\n",
    "plt.legend(loc='upper left')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "rmse = np.sqrt(mean_squared_error(df[target_next].tail(test_size).tolist(), predictions))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print('RMSE: %.5f' % rmse)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Grid search"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# evaluate combinations different parameters for XGBoost model\n",
    "def evaluate_models_xgboost(data, train_size, test, max_depth_values, n_estimators_values, learning_rate_values):\n",
    "    X_train = data[:train_size, :last]\n",
    "    y_train = data[:train_size, last]\n",
    "    X_test = data[train_size:, :last]\n",
    "    y_test = data[train_size:, last]\n",
    "    \n",
    "    best_score, best_cfg = float(\"inf\"), None\n",
    "    for max_depth in max_depth_values:\n",
    "        for n_estimators in n_estimators_values:\n",
    "            for learning_rate in learning_rate_values:\n",
    "                # train model\n",
    "                model = XGBRegressor(\n",
    "                    objective = 'reg:squarederror',\n",
    "                    max_depth = max_depth,\n",
    "                    n_estimators = n_estimators,\n",
    "                    learning_rate = learning_rate,\n",
    "                )\n",
    "                \n",
    "                # YOUR CODE HERE\n",
    "    \n",
    "    print('Best XGBoost: %s RMSE=%.5f' % (best_cfg, best_score))\n",
    "    return best_cfg, best_score"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "max_depth_values = [3, 6, 9]\n",
    "n_estimators_values = [50, 100, 150]\n",
    "learning_rate_values = [0.1, 0.25, 0.4]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# prepare data\n",
    "data = df.values\n",
    "train_size = int(data.shape[0] - test_size)\n",
    "test = df[target_next].tail(test_size).tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "best_cfg, best_score = evaluate_models_xgboost(data, train_size, test, max_depth_values, n_estimators_values, learning_rate_values)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "best_cfg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "last = data.shape[1] - 1\n",
    "X_train = data[:train_size, :last]\n",
    "y_train = data[:train_size, last]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# train model\n",
    "model = XGBRegressor(\n",
    "    objective = 'reg:squarederror',\n",
    "    max_depth = 3,\n",
    "    n_estimators = 100,\n",
    "    learning_rate = 0.25,\n",
    ")\n",
    "model.fit(X_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "rmse, predictions = evaluate_regression_model(data, model, train_size, test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "print('RMSE: %.5f' % rmse)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "len(predictions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "plt.plot([None for i in df[target_next].values[:train_size]] + [x for x in df[target_next].tail(test_size).tolist()], label=\"Expected\")\n",
    "plt.plot([None for i in df[target_next].values[:train_size]] + [x for x in predictions], label=\"Predicted\", color=\"red\")\n",
    "plt.legend(loc='upper left')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
